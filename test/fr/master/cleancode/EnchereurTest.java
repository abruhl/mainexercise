package fr.master.cleancode;

import fr.master.cleancode.exception.ChoiceArgumentException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

public class EnchereurTest {

    private static final InputStream systemIn = System.in;
    private static final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;


    private Enchereur enchereur;
    private ConsoleApplication mockedconsoleApplication;
    private Enchere enchere;



    @Before
    public void setCurrentPrice() throws NoSuchFieldException, IllegalAccessException {
        mockedconsoleApplication = Mockito.mock(ConsoleApplication.class);
        enchereur = new Enchereur(mockedconsoleApplication);
        Field currentProductPriceField = enchereur.getClass().getDeclaredField("currentProductPrice");
        currentProductPriceField.setAccessible(true);
        currentProductPriceField.setInt(enchereur,50);
    }




    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }


    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }


    /*@Test
    public void wantMakeEnchereShouldReturnTrue()
    {
        provideInput("o");
        boolean result = enchereur.wantMakeEnchere();

        verify(mockedconsoleApplication).displayActionChoiceForm();

        assertThat(result).isEqualTo(true);
    }*/

    @Test
    public void wantMakeEnchereShouldReturnFalse() throws ChoiceArgumentException {
        provideInput("n");
        boolean result = enchereur.wantMakeEnchere();

        verify(mockedconsoleApplication).displayActionChoiceForm();

        assertThat(result).isEqualTo(false);
    }

    /*@Test
    public void exceptionShouldRecallFunction()
    {
        provideInput("abc");
        enchereur.wantMakeEnchere();
        verify(mockedconsoleApplication,times(2)).displayActionChoiceForm();

        provideInput("abc");
        enchereur.makeEnchere();
        verify(mockedconsoleApplication,times(2)).displayEnchereForm();

    }*/

   /* @Test
    public void amountNotGreaterThanCurrentPriceShouldThrowException(){

        provideInput("35");
        enchereur.makeEnchere();
        provideInput("60");
        verify(mockedconsoleApplication, times(2)).displayEnchereForm();

    }*/

   /*@Test
   public void isSuperiorShouldReturnTrue()
   {

       boolean result  = enchereur.isSuperiorToCurrentPrice(60);
       assertThat(result).isEqualTo(true);
   }

    @Test
    public void isSuperiorShouldReturnFalse()
    {

        boolean result  = enchereur.isSuperiorToCurrentPrice(40);
        assertThat(result).isEqualTo(false);
    }*/


    /*@Test
    public void amountShouldBeGreaterThanCurrentPrice(){


        /*ConsoleApplication console = new ConsoleApplication();
        provideInput("60");
        int result = console.displayEnchereForm();
        assertThat(result).isEqualTo(60);

        provideInput("60");
        assertThat(enchereur).extracting("currentProductPrice").first().isEqualTo(50);



        enchereur.makeEnchere();
        //verify(mockedconsoleApplication).displayEnchereForm();
    }*/

    @Test
    public void currentPriceShouldBeUpdate()
    {
        assertThat(enchereur).extracting("currentProductPrice").first().isEqualTo(50);
        enchereur.notifyNewPrice(60);
        assertThat(enchereur).extracting("currentProductPrice").first().isEqualTo(60);
    }

    @Test
    public void currentEnchereSouldBeUpdate()
    {

    }



}