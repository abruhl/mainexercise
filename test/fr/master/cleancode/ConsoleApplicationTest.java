package fr.master.cleancode;

import fr.master.cleancode.exception.ChoiceArgumentException;
import fr.master.cleancode.exception.EnchereArgumentException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsoleApplicationTest {


    private static final InputStream systemIn = System.in;
    private static final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }


    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }



    @Test
    public void amountShouldBeInteger() throws EnchereArgumentException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        provideInput("12");
        Integer result = consoleApplication.displayEnchereForm();

        assertThat(result).isEqualTo(12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void notIntegerAmountShouldThrowException() throws EnchereArgumentException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        provideInput("abc");
        consoleApplication.displayEnchereForm();
    }


    @Test(expected = IllegalArgumentException.class)
    public void notAuthorizedChoiceShouldThrowException() throws EnchereArgumentException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        provideInput("abc");
        consoleApplication.displayEnchereForm();
    }


    @Test
    public void userCouldchoiceToNotMakeAnEnchere() throws ChoiceArgumentException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        provideInput("n");
        boolean result = consoleApplication.displayActionChoiceForm();

        assertThat(result).isEqualTo(false);

    }


    @Test
    public void userCouldchoiceToMakeAnEnchere() throws ChoiceArgumentException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        provideInput("o");
        boolean result = consoleApplication.displayActionChoiceForm();

        assertThat(result).isEqualTo(true);

    }


}