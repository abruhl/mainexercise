package fr.master.cleancode.manager;


import fr.master.cleancode.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.setAllowExtractingPrivateFields;
import static org.mockito.Mockito.verify;

public class SaleManagerTest {

    private SaleManager saleManager;
    private Enchereur enchereur;

    private static final InputStream systemIn = System.in;
    private static final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException {

        saleManager = new SaleManager(new Product(100,"lampe", "Style louis XVI en bronze"));
        Field listParticipantField = saleManager.getClass().getDeclaredField("enchereurs");
        listParticipantField.setAccessible(true);

        Field currentProductPriceField = saleManager.getClass().getDeclaredField("currentProductPrice");
        currentProductPriceField.setAccessible(true);
        currentProductPriceField.set(saleManager,10);

        enchereur = new Enchereur(new ConsoleApplication());
        Field currentOwnerField = saleManager.getClass().getDeclaredField("currentOwner");
        currentOwnerField.setAccessible(true);
        currentOwnerField.set(saleManager,enchereur);


    }


    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }


    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }


    @Test
    public void registerShouldAddEnchereur() {

        assertThat(saleManager).extracting("enchereurs.size").contains(0);

        Enchereur enchereur = new Enchereur(new ConsoleApplication());
        saleManager.registerEnchereur(enchereur);

        assertThat(saleManager).extracting("enchereurs").extracting("size").hasSize(1);

    }



    /*@Test
    public void callEnchereShouldUnregisterEnchereur()
    {
        Enchereur enchereur = new Enchereur(new ConsoleApplication());
        saleManager.registerEnchereur(enchereur);
        assertThat(saleManager).extracting("enchereurs").extracting("size").hasSize(1);
        provideInput("n");
        saleManager.callEnchere();
        assertThat(saleManager).extracting("enchereurs.size").contains(0);


    }*/

    /*@Test
    public void callEnchereShouldMakeEnchere()
    {

        Enchereur mockEnchereur = Mockito.mock(Enchereur.class);

        saleManager.registerEnchereur(mockEnchereur);
        assertThat(saleManager).extracting("enchereurs").extracting("size").hasSize(1);
        provideInput("o");
        saleManager.callEnchere();
        verify(mockEnchereur).makeEnchere();


    }*/

    @Test
    public void acceptEnchereShouldGetMaxEnchere()
    {

        Enchereur enchereur1 = new Enchereur(new ConsoleApplication());
        Enchereur enchereur2 = new Enchereur(new ConsoleApplication());
        Enchereur enchereur3 = new Enchereur(new ConsoleApplication());
        Enchere enchere1 = new Enchere(60,enchereur1);
        Enchere enchere2 = new Enchere(1000,enchereur2);
        Enchere enchere3 = new Enchere(500,enchereur3);

        List<Enchere> encheres = new ArrayList<Enchere>();
        encheres.add(enchere1);
        encheres.add(enchere2);
        encheres.add(enchere3);

        saleManager.acceptEnchere(encheres);

        assertThat(saleManager).extracting("currentProductPrice").first().isEqualTo(1000);
        assertThat(saleManager).extracting("currentOwner").first().isEqualTo(enchereur2);



    }

    @Test
    public void noEnchereShouldChangeNothing(){


        List<Enchere> encheres = new ArrayList<Enchere>();

        saleManager.acceptEnchere(encheres);

        assertThat(saleManager).extracting("currentProductPrice").first().isEqualTo(10);
        assertThat(saleManager).extracting("currentOwner").first().isEqualTo(enchereur);
    }



}