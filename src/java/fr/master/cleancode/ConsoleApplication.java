package fr.master.cleancode;

import fr.master.cleancode.exception.ChoiceArgumentException;
import fr.master.cleancode.exception.EnchereArgumentException;

import java.util.Scanner;

public class ConsoleApplication {

    private Scanner scanner;

    public ConsoleApplication() {

        this.scanner = new Scanner(System.in);
    }

    public boolean displayActionChoiceForm() throws ChoiceArgumentException{

        System.out.println("Voulez-vous faire une enchère ? o/n");

        String input = scanner.nextLine();

        if(validateChoice(input))
        {

            if(input.equals("o"))
            {

                return true;
            }
            else
            {


                return false;


            }
        }
        else
        {
            throw new ChoiceArgumentException();
        }
    }

    public Integer displayEnchereForm() throws EnchereArgumentException
    {

        System.out.println("Faites une enchère : (taper le montant)");

        String input = scanner.nextLine();


        if(validateEnchere(input))
        {
            return Integer.valueOf(input);
        }
        else
        {
            throw new EnchereArgumentException();
        }

    }

    private boolean validateChoice(String input)
    {
        if(input.matches("o|n"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean validateEnchere(String input)
    {

        if(input.matches("[0-9]+"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



}
