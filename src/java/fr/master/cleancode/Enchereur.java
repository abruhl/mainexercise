package fr.master.cleancode;

import fr.master.cleancode.exception.ChoiceArgumentException;
import fr.master.cleancode.exception.EnchereArgumentException;

public class Enchereur implements EnchereurInterface {


    private int enchereurId;
    private int currentProductPrice;
    private static int nbEnchereur = 0;
    private ConsoleApplication console;

    public Enchereur(ConsoleApplication console) {
        this.console = console;
        this.enchereurId = nbEnchereur++;
    }

    public boolean wantMakeEnchere()
    {
        try{

            return console.displayActionChoiceForm();


        }catch(ChoiceArgumentException e)
        {
            System.out.println(e.getMessage());
            Logger.logError(this.getClass().getName(),e.getMessage());
            return wantMakeEnchere();
        }

    }

    public Enchere makeEnchere(){


        try{

            int amount = console.displayEnchereForm();


            if(isSuperiorToCurrentPrice(amount)){

                return new Enchere(amount, this);
            }

        }catch (EnchereArgumentException e)
        {
            System.out.println(e.getMessage());
            Logger.logError(this.getClass().getName(),e.getMessage());

        }

        return makeEnchere();

    }

    private boolean isSuperiorToCurrentPrice(int amount) {


        return amount > currentProductPrice;


    }




    public void notifyNewPrice(int newPrice) {
        this.currentProductPrice  = newPrice;

    }

    public int getEnchereurId() {
        return enchereurId;
    }
}
