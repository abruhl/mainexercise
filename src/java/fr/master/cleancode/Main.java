package fr.master.cleancode;

import fr.master.cleancode.manager.SaleManager;

public class Main {



    public static void main(String... args){



       SaleStarter saleStarter = new SaleStarter(
               new SaleManager(new Product(20,"lampe", "Style louis XVI en bronze")),
               new Enchereur(new ConsoleApplication()),
               new Enchereur(new ConsoleApplication()),
               new Enchereur(new ConsoleApplication()));

       saleStarter.startSale();

    }
}
