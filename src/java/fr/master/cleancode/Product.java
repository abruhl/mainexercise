package fr.master.cleancode;

public class Product {

    private int startPrice;
    private String name;
    private String description;

    public Product(int startPrice, String name, String description) {
        this.startPrice = startPrice;
        this.name = name;
        this.description = description;
    }

    public int getStartPrice() {
        return startPrice;
    }

    @Override
    public String toString() {
        return "L'article  : " +
                name + ", " +
                description;
    }
}
