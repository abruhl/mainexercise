package fr.master.cleancode;

import fr.master.cleancode.manager.SaleManager;

import java.util.List;

public class SaleStarter {



    private SaleManager saleManager;


    public SaleStarter(SaleManager saleManager, Enchereur ... enchereurs) {
        this.saleManager = saleManager;
        for(Enchereur enchereur : enchereurs)
        {
            saleManager.registerEnchereur(enchereur);
        }
    }


    public void startSale()
    {
        saleManager.introduceProduct();

        enchereInteration();

        conclureResult();
    }

    public void enchereInteration(){

        while(!saleManager.getEnchereurs().isEmpty())
        {

            List<Enchere> encheres = saleManager.callEnchere();
            saleManager.acceptEnchere(encheres);
        }
    }

    public void conclureResult()
    {
        if(saleManager.getCurrentOwner() == null)
        {
            System.out.println("Le produit "+ saleManager.getProductToSell().toString()+" n'est pas vendu !");
        }
        else
        {
            System.out.println("Le produit "+ saleManager.getProductToSell().toString()+" est vendu à l'enchereur " + saleManager.getCurrentOwner().getEnchereurId() +" à "+ saleManager.getCurrentProductPrice() +" €");

        }
    }



}
