package fr.master.cleancode.manager;

import fr.master.cleancode.Enchere;
import fr.master.cleancode.Enchereur;

import java.util.ArrayList;
import java.util.List;

public class FideliteManager {

    private List<Enchere> listEnchere;
    private List<Enchereur> listEnchereur;
    final int MONTANT_GENEREUX = 4000;

    public List<Enchereur> getBestEnchereur() {
        List<Enchereur> listMeilleurEnchereur = new ArrayList<Enchereur>();
        for (Enchere enchere: listEnchere)
        {
            if (enchere.getAmount() > MONTANT_GENEREUX)
            {
                listMeilleurEnchereur.add(enchere.getEnchereur());
            }
        }
        return listMeilleurEnchereur;
    }

    public boolean isFidelEnchereur() {
        for (Enchereur enchereur : listEnchereur) {
            if (enchereur.getEnchereurId() == 1) {
                return true;
            }
        }
        return false;
    }






}
