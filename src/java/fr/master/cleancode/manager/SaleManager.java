package fr.master.cleancode.manager;

import fr.master.cleancode.Enchereur;
import fr.master.cleancode.EnchereurInterface;
import fr.master.cleancode.Enchere;
import fr.master.cleancode.Product;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SaleManager {


    private Product productToSell;
    private int currentProductPrice;
    private Enchereur currentOwner;
    private List<Enchereur> enchereurs;


    public SaleManager(Product productToSell) {
        this.productToSell = productToSell;
        this.currentOwner = null;
        this.currentProductPrice = productToSell.getStartPrice();
        this.enchereurs = new ArrayList<Enchereur>();

    }


    public void registerEnchereur(Enchereur enchereur)
    {
        enchereurs.add(enchereur);
    }



    public void notifyPriceChange()
    {
        for(Enchereur enchereur : this.enchereurs)
        {
            enchereur.notifyNewPrice(currentProductPrice);
        }
    }

    public void introduceProduct()
    {
        System.out.println(productToSell.toString());
        System.out.println(productToSell.getStartPrice()+" €");

        notifyPriceChange();
    }

    public List<Enchere> callEnchere() {

        List<Enchere> encheres = new ArrayList<Enchere>();

        Iterator<Enchereur> iter = this.enchereurs.iterator();

        while (iter.hasNext()) {
            Enchereur currentEnchereur = iter.next();

            System.out.println("Enchéreur n° "+currentEnchereur.getEnchereurId());

            if(currentEnchereur.wantMakeEnchere())
            {
                Enchere newEnchere = currentEnchereur.makeEnchere();

                encheres.add(newEnchere);
            }
            else
            {
                iter.remove();
            }

        }

        return encheres;
    }

    public void acceptEnchere(List<Enchere> encheres){

        if(!encheres.isEmpty())
        {
            Enchere enchere = getMaxEnchere(encheres);
            this.currentOwner = enchere.getEnchereur();
            this.currentProductPrice = enchere.getAmount();

            System.out.println("Le prix est de "+ currentProductPrice);

            notifyPriceChange();

        }


    }

    private Enchere getMaxEnchere(List<Enchere> encheres)
    {

        Enchere enchereMax = null;
        int max = 0;

        for(Enchere enchere : encheres)
        {
            if(enchere.getAmount()> max)
            {
                max = enchere.getAmount();
                enchereMax = enchere;
            }
        }

        return enchereMax;
    }


    public List<Enchereur> getEnchereurs() {
        return enchereurs;
    }

    public Enchereur getCurrentOwner() {
        return currentOwner;
    }

    public int getCurrentProductPrice() {
        return currentProductPrice;
    }

    public Product getProductToSell() {
        return productToSell;
    }
}
