package fr.master.cleancode;

public class Enchere {
    private static int id = 0;
    private int amount;
    private Enchereur enchereur;


    public Enchere( int amount, Enchereur enchereur) {
        this.id++;
        this.amount = amount;
        this.enchereur = enchereur;
    }

    public int getAmount() {
        return amount;
    }

    public Enchereur getEnchereur() {
        return enchereur;
    }
}
