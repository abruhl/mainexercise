package fr.master.cleancode;

public interface EnchereurInterface {

    Enchere makeEnchere();
    boolean wantMakeEnchere();
    void notifyNewPrice(int newPrice);


}
