package fr.master.cleancode.exception;

public class ChoiceArgumentException extends Exception {

    public ChoiceArgumentException(String message) {
        super(message);
    }

    public ChoiceArgumentException() {
        super("L'entré doit être égale à \"o\" ou \"n\"");
    }
}
