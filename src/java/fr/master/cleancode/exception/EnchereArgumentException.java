package fr.master.cleancode.exception;

public class EnchereArgumentException extends Exception{
    public EnchereArgumentException(String message) {
        super(message);
    }

    public EnchereArgumentException() {
        super("L'entrée doit être un entier");
    }
}
